import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  public authToken;
  private isAuthenticated = true; // Set this value dynamically

  constructor(private router: Router,
    private auth: AuthService,

  ) {

  }
  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  //   if (this.isAuthenticated) {
  //     return true
  //   }
  //   this.router.navigate(['/sessions/signin']);
  //   return false;
  // }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const uid = await this.auth.uid();
    const master = await this.auth.masters();
    const counselorUid = await this.auth.counselorUid();
    const counselorMaster = await this.auth.counselorMasters();
    console.log('master', master);
    console.log('uid', uid);
    console.log('counselorMaster', counselorMaster);
    console.log('counselorUid', counselorUid);

    const isLoggedIn = !!uid;
    const isCounselorIn = !!counselorUid;

    if (!isLoggedIn && !isCounselorIn) {
      console.log('is not log in');

      // const alert = await this.alertController.create({
      //   header: 'Blocked',
      //   subHeader: 'Users only',
      //   message: 'You have been blocked by the router guard...',å
      //   buttons: ['OK']
      // });

      // await alert.present();
      this.router.navigate(['sessions/signin'], { queryParams: { returnUrl: state.url } });
    }else if(isLoggedIn){
      return isLoggedIn;
    }else if(isCounselorIn){
      return(isCounselorIn);
    }
    
  }
}