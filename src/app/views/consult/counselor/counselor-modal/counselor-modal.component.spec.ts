import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounselorModalComponent } from './counselor-modal.component';

describe('CounselorModalComponent', () => {
  let component: CounselorModalComponent;
  let fixture: ComponentFixture<CounselorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounselorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounselorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
