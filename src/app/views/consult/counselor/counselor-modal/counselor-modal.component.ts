import { AppLoaderService } from './../../../../shared/services/app-loader/app-loader.service';
import { FcmService } from './../../../../services/fcm.service';
import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { DbService } from 'app/services/db.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatDialog } from '@angular/material';
import { CommonService } from 'app/services/common.service';
import { jsPDF } from 'jspdf';
import { map, take } from 'rxjs/operators';
import * as firebase from 'firebase';
import * as moment from 'moment';
import {
  NanumGothicBold,
  NanumGothicRegular,
} from '../../../../../assets/fonts/default_vfs';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as pdfMakeConfig from 'pdfmake/build/pdfmake.js';


@Component({
  selector: 'app-counselor-modal',
  templateUrl: './counselor-modal.component.html',
  styleUrls: ['./counselor-modal.component.scss'],
})

// 메뉴명 : 분석관리 > 상담서 보기
export class CounselorModalComponent implements OnInit {

  url;
  items$;
  result;
  guide;
  images: any;
  userId: string = '';
  urlList = [];
  nickname;
  resultList = [];
  counselorId;
  failAnalyze: boolean = false;
  detailResult;
  counselorDate: string = '';
  writtenSwitch: boolean = false;
  file;
  divQuestion:boolean=false;
  questionList$;

  constructor(
    public db: DbService,
    private common: CommonService,
    public dialogRef: MatDialogRef<CounselorModalComponent>,
    private fcm: FcmService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public load: AppLoaderService,
  ) {
  }
  
  ngOnInit() {
    this.counselorId = this.data.counselorId;
    this.getData();
    pdfMakeConfig.fonts = {
      NanumGothic: {
        normal: 'NanumGothicBold',
        bold: 'NanumGothicBold.ttf',
      },
    };
    this.getText();
  }

  //상담서와 결과서 데이터 가져오는 함수
  getData() {
    this.items$ = this.db.doc$(`counselor/${this.counselorId}`).pipe(
      map((item) => {
        this.failAnalyze = item.failAnalyze;
        this.userId = item.uid;
        this.getUserData(this.userId);
        this.images = item.imageList;
        this.questionList$ = item.questionList;
        this.counselorDate = moment(item.dateCreated).format('YYYY-MM-DD');
        return item;
      })
    );

    this.db
      .collection$(`feedback`, (ref) =>
        ref.where('counselorId', '==', this.counselorId)
      )
      .pipe(take(1))
      .subscribe((datas: any) => {
        if (datas.length > 0) {
          this.writtenSwitch = true;
          this.resultList.push({ title: '분석결과', content: datas[0].result });
          this.resultList.push({ title: '상세분석', content: datas[0].detailResult });
          this.resultList.push({ title: '치료 가이드', content: datas[0].guide });
          this.result = datas[0].result;
          this.result = datas[0].result;
          this.detailResult = datas[0].detailResult;
          this.guide = datas[0].guide;
        }
      });
  }

  //유저 정보가져오는 함수
  getUserData(uid) {
    this.db
      .doc$(`users/${uid}`)
      .pipe(take(1))
      .subscribe((user) => {
        this.nickname = user.nickname;
      });
  }

  //PDF 변환 함수
  getEstimate() {
    this.load.open();
    const doc = new jsPDF();
    doc.addFileToVFS('NanumGothic-Bold.ttf', NanumGothicBold);
    doc.addFileToVFS('NanumGothic-Regular.ttf', NanumGothicRegular);
    doc.addFont('NanumGothic-Bold.ttf', 'NanumGothic-Bold', 'normal');
    doc.addFont('NanumGothic-Regular.ttf', 'NanumGothic-Regular', 'normal');

    doc.setFont('NanumGothic-Bold');

    doc.setFontSize(22);
    doc.text(`${this.nickname}의 상담 결과서`, 15, 20);
    doc.setFont('NanumGothic-Regular');
    doc.setFontSize(11);
    doc.text(`상담일자: ${this.counselorDate}`, 15, 35);
    doc.setFontSize(13);
    var lMargin = 15; //left margin in mm
    var rMargin = 15; //right margin in mm
    var pdfInMM = 210;  // width of A4 in mm
    this.imagePushFor().then(() => {
      doc.addImage(this.urlList[0], 20, 50, 80, 80);
      doc.addImage(this.urlList[1], 110, 50, 80, 80);
      doc.addImage(this.urlList[2], 20, 130, 80, 80);
      doc.addImage(this.urlList[3], 110, 130, 80, 80);

      doc.addPage();
      doc.setFontSize(20);
      doc.text('분석 결과', lMargin, 20);
      doc.setFontSize(13);
      var paragraph = this.resultList[0].content;
      var lines = doc.splitTextToSize(paragraph, (pdfInMM - lMargin - rMargin));
      doc.text(lines, lMargin, 40);

      doc.addPage();
      doc.setFontSize(20);
      doc.text('상세 결과', lMargin, 20);

      doc.setFontSize(13);
      var paragraph = this.resultList[1].content;
      var lines = doc.splitTextToSize(paragraph, (pdfInMM - lMargin - rMargin));
      doc.text(lines, lMargin, 40);

      doc.addPage();
      doc.setFontSize(20);
      doc.text('치료 가이드', lMargin, 20);

      doc.setFontSize(13);
      var paragraph = this.resultList[2].content;
      var lines = doc.splitTextToSize(paragraph, (pdfInMM - lMargin - rMargin));
      doc.text(lines, lMargin, 40);

      doc.save('picture_feedback_auto.pdf');
      this.load.close();
    });
  }

  //이미지 출력 함수
  imagePushFor() {
    return new Promise((resolve, reject) => {
      let cnt = 0;
      this.images.forEach((element) => {
        this.getBase64ImageFromURL(element.url)
          .then((a: any) => {
            this.urlList.push(a);
            cnt++;

            if (cnt == 4) {
              resolve(this.urlList);
            }
          })
          .catch((error) => {
            console.log('error', error);
            reject(error);
          });
      });
    });
  }

  //결과서 작성하는 함수
  submit() {
    this.db.doc$(`counselor/${this.counselorId}`).pipe(take(1)).subscribe((data: any) => {
      if (!data.feedbackId && !data.analysisSwitch) {
        let feedback = {
          feedbackId: this.common.generateFilename(),
          dateCreated: new Date().toISOString(),
          counselorId: this.counselorId,
          result: this.result,
          detailResult: this.detailResult,
          guide: this.guide,
        };
        this.db.updateAt(`feedback/${feedback.feedbackId}`, feedback);
        this.db
          .updateAt(`counselor/${this.counselorId}`, {
            feedbackId: feedback.feedbackId,
            analysisSwitch: true,
          })
          .then((success) => {
            this.writtenSwitch = true;
            this.addAlarm(data.uid, true);
            this.db
              .doc$(`users/${this.userId}`)
              .pipe(take(1))
              .subscribe((user) => {
                if (user && user.pushId && user.alarmSwitch) {
                  this.fcm.sendPage(
                    user.pushId,
                    '그림심리상담',
                    this.finish,
                    this.counselorId
                  );
                }
              });
          })
          .catch((error) => {
            console.log('error', error);
          });
      } else {
        this.db.updateAt(`feedback/${data.feedbackId}`, {
          result: this.result,
          detailResult: this.detailResult,
          guide: this.guide,
        });
      }
      this.dialogRef.close();
      return data;
    });
  }

  //결과서 작성시 알림멘트 설정 함수
  cant;
  finish;
  getText() {
    this.db.doc$(`master/push`).pipe(take(1)).subscribe(data => {
      this.finish = data.finish;
      this.cant = data.cant;
    })
  }

  //알림추가 함수
  addAlarm(uid: any, isOk?) {
    const text = isOk
      ? this.finish
      : this.cant;

    let alarm = {
      userId: uid,
      text: '',
      check: false,
      dateCreated: new Date().toISOString(),
    };

    if (isOk) {
      alarm.text = text;
      this.db.updateAt(`alarm`, alarm);
    } else {
      alarm.text = text;
      this.db.updateAt(`alarm`, alarm);
      this.db
        .updateAt(`counselor/${this.counselorId}`, {
          failAnalyze: true,
        })
        .then(() => {
          this.failAnalyze = true;
          this.db
            .doc$(`users/${this.userId}`)
            .pipe(take(1))
            .subscribe((user) => {
              if (user && user.pushId && user.alarmSwitch) {
                this.fcm.sendPage(
                  user.pushId,
                  '그림심리상담',
                  this.cant,
                  this.counselorId
                );
              }
            });
          this.dialogRef.close();
        });
    }
  }

  //이미지 변환 함수
  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      // img.style.padding = '5.669291338583px';
      img.onload = () => {
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);

        const dataURL = canvas.toDataURL('image/png', 1.0);
        resolve(dataURL);
      };

      img.onerror = (error) => {
        reject(error);
      };

      img.src = url;
    });
  }

  //결과서 작성시, 파일 업로드 해주는 함수
  hidBtn:boolean=false;
  uploadFile: any;
  selectFile(event) {
    var reader = new FileReader();
    this.uploadFile = event.target.files[0];
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = _event => {
      this.url = reader.result;
      console.log('-Evnet', _event);
      const storageRef = firebase.storage().ref();
      const fileName = 'file' + moment().format('x');
      const upRef = 'file/' + fileName + '.pdf';
      const uploadTask = storageRef.child(upRef).putString(this.url, 'data_url');
      uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        snapshot => {
          console.log('snapshot', snapshot);
        },
        error => {
          console.log('error', error);
        },
        () => {
          firebase
            .storage()
            .ref()
            .child(upRef)
            .getDownloadURL()
            .then(
              downloadURL => {
                this.db.updateAt(`counselor/${this.counselorId}`, {
                  pdfUrl: downloadURL,
                });
              },
              e => {
                console.log('error', e);
              }
            )
            .then(
              () => { },
              e => {
                console.log('error', e);
              }
            );
        }
      );
    };
    
    this.hidBtn = true;
  }

  closeDialog(){
    this.dialogRef.close();
  }

  cancelUpload(){
    this.db.updateAt(`counselor/${this.counselorId}`,{
      pdfUrl:'',
    });
    this.file= null;
    this.hidBtn=false;
  }

  openQuestionList(hide){
    this.divQuestion = hide;
  }
}
