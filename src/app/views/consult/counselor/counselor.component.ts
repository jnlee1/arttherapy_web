import { CounselorModalComponent } from './counselor-modal/counselor-modal.component';
import { leftJoinDocument } from './../../../services/db.service';
import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { DbService } from 'app/services/db.service';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { ProfileModalComponent } from './profile-modal/profile-modal.component';
import { take } from 'lodash';
import { FcmService } from 'app/services/fcm.service';

@Component({
  selector: 'app-counselor',
  templateUrl: './counselor.component.html',
  styleUrls: ['./counselor.component.scss'],
  animations: egretAnimations
})

// 메뉴명 : 상담사용 분석관리
export class CounselorComponent implements OnInit {
  items$;
  public items: any[];
  disabled:boolean=false;
  nopassbook:boolean=false;

  constructor(
    private db: DbService,
    private dialog: MatDialog,
    public dataProvider: DbService,
    public snack: MatSnackBar,
    public afs: AngularFirestore,
    public loader: AppLoaderService,
    public confirmService: AppConfirmService,
    private fcm :FcmService) { }

  ngOnInit() {
    this.getCounselor();

  }

  //신청된 상담서들 불러오는 함수
  getCounselor() {
    this.items$ = this.db.collection$(`counselor`, ref => ref.where('step','==','/tabs/home-add-draw-result').orderBy("dateCreated", "desc"))
      .pipe(leftJoinDocument(this.db.afs, 'uid', 'users'))
  }

  //상담서 분석관리에서 유저들 정보 보여주는 함수
  profile(data: any, isNew?) {
    const title = isNew ? 'User Info' : 'Error';

    const dialogRef: MatDialogRef<any> = this.dialog.open(ProfileModalComponent, {
      width: '800px',
      height: '800px',
      disableClose: false,
      data: { title: title, payload: data, user: data }
    });
    // 수정 전
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.loader.open();
      if (isNew) {
        this.dataProvider.getUserDetail(res.id).valueChanges().take(1).subscribe(data2 => {
          this.loader.close();
          // this.snack.open('User Detail!', 'OK', { duration: 3000 });
        });
      }
    });
  }

  //상담서 상세보기 함수
  showCounselor(counselorId) {
    const dialogRef: MatDialogRef<any> = this.dialog.open(CounselorModalComponent, {
      width: '800px',
      height: '800px',
      disableClose: false,
      data: { counselorId: counselorId }
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.loader.open();

      this.dataProvider.getUserDetail(res.id).valueChanges().take(1).subscribe(data2 => {
        this.loader.close();
        // this.snack.open('User Detail!', 'OK', { duration: 3000 });
      });
    }

    );
  }

  //결제 확인
  checkPay(event,docId){
    if(event == false){
      if(confirm('검사료 입금을 확인하셨습니까?') == true){
       this.db.updateAt(`counselor/${docId}`,{pay:true});
      }else{
        return false;
      }
    }
  }

  //무통장입금 정보 푸시
  sendInfo(user){
    let alarm ={
      userId : user.uid,
      text : '은행명 : 신한은행\n예금주 : (주)제이앤리\n계좌번호 : 100-034-687520\n검사료 : 30,000원\n\n감사합니다.',
      check:false,
      dateCreated:new Date().toISOString()
    };

    this.db.updateAt(`alarm`,alarm).then(()=>{
      this.fcm.sendFcm(
        user.pushId,
        '그림검사 결제 정보',
        alarm.text
      );
    });
  }


}
