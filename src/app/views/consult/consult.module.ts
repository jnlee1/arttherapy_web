import { CounselorComponent } from './counselor/counselor.component';
import { ConsultRoutes } from './consult.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedMaterialModule } from 'app/shared/shared-material.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { AdminCounselorComponent } from './admin-counselor/admin-counselor.component';

import {
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatChipsModule,
  MatListModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSlideToggleModule
} from '@angular/material';
import { ProfileModalComponent } from './counselor/profile-modal/profile-modal.component';
import { CounselorModalComponent } from './counselor/counselor-modal/counselor-modal.component';
@NgModule({
  declarations: [
    CounselorComponent,
    ProfileModalComponent,
    CounselorModalComponent,
    AdminCounselorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(ConsultRoutes),
    NgxDatatableModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    TranslateModule,
    NgbModule,
  ],
  entryComponents: [
    ProfileModalComponent,
    CounselorModalComponent
  ]
})
export class ConsultModule { }
