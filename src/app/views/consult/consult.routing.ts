import { CounselorComponent } from './counselor/counselor.component';
import { AdminCounselorComponent } from './admin-counselor/admin-counselor.component';
import { Routes } from '@angular/router';


export const ConsultRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'admin-counselor',
        component: AdminCounselorComponent,
        data: { title: '분석 관리', breadcrumb: '분석 관리' }
      },
      {
        path: 'counselor',
        component: CounselorComponent,
        data: { title: '분석 관리', breadcrumb: '분석 관리' }
      },
    ],
  },


];