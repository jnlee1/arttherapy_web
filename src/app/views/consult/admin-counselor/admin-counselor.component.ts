import { CounselorModalComponent } from '../counselor/counselor-modal/counselor-modal.component';
import { leftJoinDocument } from './../../../services/db.service';
import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { DbService } from 'app/services/db.service';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { ProfileModalComponent } from '../counselor/profile-modal/profile-modal.component';
import { take } from 'lodash';
import { FcmService } from 'app/services/fcm.service';

@Component({
  selector: 'app-admin-counselor',
  templateUrl: './admin-counselor.component.html',
  styleUrls: ['./admin-counselor.component.scss'],
  animations: egretAnimations
})

// 메뉴명 : 관리자용 분석관리
export class AdminCounselorComponent implements OnInit {
  items$;
  public items: any[];
  disabled:boolean=false;
  nopassbook:boolean=false;

  constructor(
    private db: DbService,
    private dialog: MatDialog,
    public dataProvider: DbService,
    public snack: MatSnackBar,
    public afs: AngularFirestore,
    public loader: AppLoaderService,
    public confirmService: AppConfirmService,
    private fcm :FcmService) { }

  ngOnInit() {
    this.getCounselor();

  }

  //신청된 상담서들 불러오는 함수
  getCounselor() {
    this.items$ = this.db.collection$(`counselor`, ref => ref.orderBy("dateCreated", "desc"))
      .pipe(leftJoinDocument(this.db.afs, 'uid', 'users'))
  }

  //상담서 분석관리에서 유저들 정보 보여주는 함수
  profile(data: any, isNew?) {
    const title = isNew ? 'User Info' : 'Error';

    const dialogRef: MatDialogRef<any> = this.dialog.open(ProfileModalComponent, {
      width: '800px',
      height: '800px',
      disableClose: false,
      data: { title: title, payload: data, user: data }
    });
    // 수정 전
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.loader.open();
      if (isNew) {
        this.dataProvider.getUserDetail(res.id).valueChanges().take(1).subscribe(data2 => {
          this.loader.close();
          // this.snack.open('User Detail!', 'OK', { duration: 3000 });
        });
      }
    });
  }

  //상담서 상세보기 함수
  showCounselor(counselorId) {
    const dialogRef: MatDialogRef<any> = this.dialog.open(CounselorModalComponent, {
      width: '800px',
      height: '800px',
      disableClose: false,
      data: { counselorId: counselorId }
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.loader.open();

      this.dataProvider.getUserDetail(res.id).valueChanges().take(1).subscribe(data2 => {
        this.loader.close();
        // this.snack.open('User Detail!', 'OK', { duration: 3000 });
      });
    }

    );
  }

  //결제 확인
  checkPay(event,docId,user){
    if(event == false){
      if(confirm('검사료 입금을 확인하셨습니까?') == true){
       this.db.updateAt(`counselor/${docId}`,{pay:true});
       this.sendCheck(user);
      }else{
        return false;
      }
    }
  }

  //무통장입금 정보 푸시
  sendInfo(user){
    let alarm ={
      userId : user.uid,
      text : '은행명 : 신한은행\n예금주 : (주)제이앤리\n계좌번호 : 100-034-687520\n검사료 : 30,000원\n\n감사합니다.',
      check:false,
      dateCreated:new Date().toISOString()
    };

    this.db.updateAt(`alarm`,alarm).then(()=>{
      this.fcm.sendFcm(
        user.pushId,
        '그림검사 결제 정보',
        alarm.text
      );
    });
  }

  
  //무통장입금 정보 푸시
  sendCheck(user){
    let alarm ={
      userId : user.uid,
      text : '입금 확인 되었습니다. 검사진행하세요.^^',
      check:false,
      dateCreated:new Date().toISOString()
    };

    this.db.updateAt(`alarm`,alarm).then(()=>{
      this.fcm.sendFcm(
        user.pushId,
        '그림검사 입금 확인',
        alarm.text
      );
     alert('검사자에게 입금 완료 푸시를 보냈습니다.');
    });
  }


}
