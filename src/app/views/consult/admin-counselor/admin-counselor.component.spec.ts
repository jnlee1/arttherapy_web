import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCounselorComponent } from './admin-counselor.component';

describe('AdminCounselorComponent', () => {
  let component: AdminCounselorComponent;
  let fixture: ComponentFixture<AdminCounselorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCounselorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCounselorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
