import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { ThemeService } from 'app/shared/services/theme.service';
import tinyColor from 'tinycolor2';
import { DbService } from 'app/services/db.service';
import * as moment from 'moment';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-default-dashboard',
  templateUrl: './default-dashboard.component.html',
  styleUrls: ['./default-dashboard.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 대시 보드
export class DefaultDashboardComponent implements OnInit {
  weekUsers$;
  lineData1: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  lineChartSteppedData: Array<any> = [{
    data: this.lineData1,
    label: '회원',
    borderWidth: 0,
    fill: true,
  }
  ];
  public lineChartLabels: Array<any> = [];
  /*
  * Full width Chart Options
  */
  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
      position: 'bottom'
    },
    scales: {
      xAxes: [{
        display: false,
        gridLines: {
          color: 'rgba(0,0,0,0.02)',
          zeroLineColor: 'rgba(0,0,0,0.02)'
        }
      }],
      yAxes: [{
        display: false,
        gridLines: {
          color: 'rgba(0,0,0,0.02)',
          zeroLineColor: 'rgba(0,0,0,0.02)'
        },
        ticks: {
          beginAtZero: true,
          suggestedMax: 9,
        }
      }]
    }
  };

  public lineChartColors: Array<any> = [];
  public lineChartLegend: boolean = false;
  public lineChartType: string = 'line';

  // Chart grid options
  doughnutChartColors1: any[] = [{
    backgroundColor: ['#fff', 'rgba(0, 0, 0, .24)',]
  }];
  doughnutChartColors2: any[] = [{
    backgroundColor: ['rgba(0, 0, 0, .5)', 'rgba(0, 0, 0, .15)',]
  }];
  total1: number = 500;
  data1: number = 200;
  doughnutChartData1: number[] = [this.data1, (this.total1 - this.data1)];

  total2: number = 600;
  data2: number = 400;
  doughnutChartData2: number[] = [this.data2, (this.total2 - this.data2)];
  doughnutLabels = ['가입', '탈퇴']
  doughnutChartType = 'doughnut';
  doughnutOptions: any = {
    cutoutPercentage: 85,
    responsive: true,
    legend: {
      display: false,
      position: 'bottom'
    },
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    tooltips: {
      enabled: true
    }
  };
  users = [];
  family = [];
  careWorker = [];
  facility = [];
  exitUsers = [];
  exitFamily = [];
  exitCareWorker = [];
  exitFacility = [];
  monthFamily = [];
  monthCareWorker = [];
  monthFacility = [];
  monthUser = [];
  lastmonthUser = [];
  weekUser = [];
  increaseUser = 0;
  increaseWeek = 0;
  increaseCareWorker = 0;
  increaseFacility = 0;

  inquiryConfirms: any = [];
  inquiryNotConfirms: any = [];

  posts: any = [];
  recruitments: any = [];

  reportPosts: any = [];
  reportReplys: any = [];

  dData2: number[] = [this.careWorker.length, this.exitCareWorker.length];
  dData3: number[] = [this.facility.length, this.exitFacility.length];
  dData1: number[] = [this.family.length, this.exitFamily.length]
  dData4: number[] = [this.users.length, this.exitUsers.length];

  constructor(
    private themeService: ThemeService,
    private db: DbService
  ) { }

  //대시보드에 나타나는 정보들 가져오는 함수
  ngOnInit() {
    let sunday = moment().weekday(0); // last Sunday
    let now = new Date().getTime();

    this.weekUsers$ = this.db.collection$(`users`, ref => ref.where('exitSwitch', '==', false))
      .pipe(map(users => {
        return users.filter((ele: any) => new Date(ele.dateCreated).getTime() >= sunday.valueOf() && new Date(ele.dateCreated).getTime() <= now)
      }));
    this.db.collection$(`users`).take(1).subscribe((users) => {
      if (users) {
        this.setData(users);
      }
    });

    var num = -1
    for (let i = 0, j = 15; i < j; i++) {
      var datelavel = moment().add(num, 'days').format('MM-DD')
      this.lineChartLabels.unshift(datelavel);
      ++num
    }
  }

  //유저정보 셋팅
  setData(users) {
    users.forEach(user => {
      var num = -1;

      for (let i = 0, j = 15; i < j; i++) {
        var datejoin = parseInt(moment(new Date(user.dateCreated)).format("YYYYMMDD"));
        var thedate = parseInt(moment().add(num, 'days').format('YYYYMMDD'));
        num--
        if (datejoin == thedate) {
          ++this.lineData1[i]
        }
      }

      this.lineChartSteppedData = [{
        data: this.lineData1,
        label: '회원',
        borderWidth: 0,
        fill: true,
      }];

      var createdMonts = parseInt(moment(new Date(user.dateCreated)).format("YYYYMM"));
      var thisMonth = parseInt(moment().format("YYYYMM"));
      var lastMonth = parseInt(moment().add(-1, 'months').format("YYYYMM"));
      var lastMuser = 0;
      if (user.exitSwitch) {
        this.exitUsers.push(user);
      } else {

      }

      this.users.push(user);
      if (createdMonts == thisMonth) {
        this.monthUser.push(user);
      } else if (createdMonts == lastMonth) {
        ++lastMuser
      }

      let sunday = moment().weekday(0); // last Sunday
      if (moment(user.dateCreated) >= sunday && moment(user.dateCreated) <= moment()) {
        this.weekUser.push(user);
      }

      this.dData4 = [this.users.length, this.exitUsers.length];

      this.users.sort((a, b) => {
        var d1 = new Date(a.dateCreated);
        var d2 = new Date(b.dateCreated);
        return (d1 > d2) ? -1 : ((d2 > d1) ? 1 : 0);
      });

      this.increaseUser = Math.ceil((lastMuser / this.monthUser.length) * 100);
    });
  }

}
