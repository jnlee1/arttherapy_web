import { Routes } from "@angular/router";
import { DefaultDashboardComponent } from "./default-dashboard/default-dashboard.component";

export const DashboardRoutes: Routes = [
  {
    path: "default",
    component: DefaultDashboardComponent,
    data: { title: "대시보드", breadcrumb: "대시보드" }
  }
];
