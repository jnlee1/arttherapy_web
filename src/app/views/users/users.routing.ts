import { Routes } from '@angular/router';

import { FamilyComponent } from './family/family.component';

export const UsersRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user',
        component: FamilyComponent,
        data: { title: '회원 관리', breadcrumb: '회원 관리' }
      },

    ],
  },


];