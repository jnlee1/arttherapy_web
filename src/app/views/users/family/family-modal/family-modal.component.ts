import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { DbService } from '../../../../services/db.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-family-modal',
  templateUrl: './family-modal.component.html',
  styleUrls: ['./family-modal.component.scss']
})

//메뉴명 : 회원관리 > 이름 프로필
export class FamilyModalComponent implements OnInit {

  public itemForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dataProvider: DbService,
    public dialogRef: MatDialogRef<FamilyModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this.buildItemForm(this.data.payload, this.data.user);
  }

  //유저 기본 정보 셋팅
  buildItemForm(item, user) {
    this.itemForm = this.fb.group({
      profile: [item.profile || 'assets/imgs/message_default.png'],
      dateCreated: [item.dateCreated || ''],
      email: [item.email || ''],
      exitSwitch: [item.exitSwitch || false],
      id: [item.id || ''],
      marketing: [user.marketing || false],
      nickname: [user.nickname || ''],
      phone: [user.phone || ''],
      birth: [user.birth || '']
    });
  }

}
