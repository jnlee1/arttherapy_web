import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MatDialog, MatSnackBar, MatSlideToggleChange } from '@angular/material';
import { DbService } from '../../../services/db.service';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { FamilyModalComponent } from './family-modal/family-modal.component';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  animations: egretAnimations
})

//메뉴명 : 회원관리 목록
export class FamilyComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public items: any[];
  public getItemSub;
  public useDefault;
  items$;

  constructor(
    public dataProvider: DbService,
    private dialog: MatDialog,
    private db: DbService,
    public snack: MatSnackBar,
    public loader: AppLoaderService,
    public confirmService: AppConfirmService
  ) { }

  ngOnInit() {
    this.getFamilyItems();
  }

  ngOnDestroy() {
  }

  //유저 정보 가져오는 함수
  getFamilyItems() {
    this.items$ = this.db.collection$(`users`, ref => ref.orderBy("dateCreated", "desc"))
  }

  //유저의 상세정보 띄어주는 창 불러오는 함수
  openPop(data: any, isNew?) {
    const title = isNew ? 'User Info' : 'Error';

    const dialogRef: MatDialogRef<any> = this.dialog.open(FamilyModalComponent, {
      width: '800px',
      height: '800px',
      disableClose: false,
      data: { title: title, payload: data, user: data }
    });
    // 수정 전
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.loader.open();
      if (isNew) {
        this.dataProvider.getUserDetail(res.id).valueChanges().take(1).subscribe(data2 => {
          this.loader.close();
        });
      }
    });
  }

  //탈퇴여부 표시해주는 함수
  toggle(event: MatSlideToggleChange, row) {
    this.useDefault = event.checked;
    this.dataProvider.getUser(row.id).update({ exitSwitch: event.checked }).then(() => {
    });
  }

}
