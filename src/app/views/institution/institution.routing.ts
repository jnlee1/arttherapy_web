import { InstitutionListComponent } from './institution-list/institution-list.component';
import { InstitutionUpdateComponent } from './institution-update/institution-update.component'
import { Routes } from '@angular/router';
import { InstitutionInsertComponent } from "./institution-insert/institution-insert.component";

export const InstitutionRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'institution-list',
        component: InstitutionListComponent,
        data: { title: '방문기관관리', breadcrumb: '방문기관관리' }
      },
      {
        path: 'institution-update',
        component: InstitutionUpdateComponent,
        data: { title: '방문기관 수정', breadcrumb: '방문기관 수정' }
      },
      {
        path: 'institution-insert',
        component: InstitutionInsertComponent,
        data: { title: '방문기관 등록', breadcrumb: '방문기관 등록' }
      }
    ],
  },


];