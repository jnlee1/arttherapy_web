//import { CounselorModalComponent } from '../counselor/counselor-modal/counselor-modal.component';
import { Component, OnInit } from '@angular/core';
import { DbService } from 'app/services/db.service';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-institution-list',
  templateUrl: './institution-list.component.html',
  styleUrls: ['./institution-list.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 방문 기관 관리 > 기관 목록
export class InstitutionListComponent implements OnInit {
  items$;
  public items: any[];

  constructor(
    private db: DbService,
    private dialog: MatDialog,
    public dataProvider: DbService,
    public snack: MatSnackBar,
    public afs: AngularFirestore,
    public loader: AppLoaderService,
    private router:Router,) { }

  ngOnInit() {
    this.getInstitution();

  }

  //기관조회
  getInstitution() {
    this.items$ = this.db.collection$(`institution`, ref => ref.orderBy("name", "desc"))
  }

  // 상세내용 수정화면으로
  goUpdate(docId){
    this.router.navigate(['/institution/institution-update'],{
      queryParams:{
        docId:docId
      }
    });
  }

  //기관등록
  goInsert(){
    this.router.navigate(['/institution/institution-insert']);
  }

}
