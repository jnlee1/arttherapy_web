import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DbService } from 'app/services/db.service';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { InstitutionService } from "../../../services/institution.service";
import * as firebase from 'firebase';
import * as moment from 'moment';

@Component({
  selector: 'app-institution-update',
  templateUrl: './institution-update.component.html',
  styleUrls: ['./institution-update.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 방문 기관 관리 > 기관 수정
export class InstitutionUpdateComponent implements OnInit {

  docId;
  testKind;
  homepage;
  email;
  number;
  address;
  institution;
  name;
  img;
  counselor;
  url



  constructor(public router:Router,
              public route:ActivatedRoute,
              public db:DbService,
              private institutionService:InstitutionService) { 
      this.institution = this.institutionService.getInstitution();
      this.route.queryParams.subscribe(params => {
        this.docId = params["docId"];
        this.getInstitution();
      });
      
    }
    
    ngOnInit() {
    }
  
    //기관정보 조회
    getInstitution(){
      this.db.doc$(`institution/${this.docId}`).subscribe((info:any)=>{
        if(info){
          this.testKind = info.kind;
          this.homepage = info.homepage;
          this.email =info.email;
          this.number = info.pNumber;
          this.address = info.address;
          this.name = info.name;
          this.img = info.img;
          this.counselor = info.counselor;
        }
      });
    }
  
    //목록으로 이동
    goInstitutionList(){
      this.router.navigate(['/institution/institution-list']);
    }

    //기관사진 선택
    selectImg(){
      document.getElementById('visitUploader').click();
    }

    //상담사 사진 선택
    selectCslrImg(){
      document.getElementById('cslrUploader').click();
    }

  
    //수정내용저장
    setUpdate(){
      if(!this.testKind){
        alert('대표검사 종류를 입력하세요.');
        document.getElementById('testKind').focus();
        return;
      }else if(!this.number){
        alert('연락처를 입력하세요.');
        document.getElementById('number').focus();
        return;
      }else if(!this.address){
        alert('주소를 입력하세요.');
        document.getElementById('address').focus();
        return;
      }

      this.institution.kind = this.testKind;
      this.institution.homepage = this.homepage;
      this.institution.email = this.email;
      this.institution.pNumber = this.number;
      this.institution.address = this.address;
      this.institution.img = this.img;
      this.institution.counselor = this.counselor;
      this.institution.updated=new Date().toISOString();
      this.db.updateAt(`institution/${this.docId}`,this.institution).then(()=>{
        alert('수정 되었습니다. 목록으로 이동합니다.');
        this.router.navigate(['/institution/institution-list']);
        return;
      }).catch((error:any)=>{
        alert('새로고침 후 다시시도하세요.\n에러 발생 : ' + error);
        return;
      });
    }

  
//기관사진 업로드
selectFile(event) {
  var reader = new FileReader();
  reader.readAsDataURL(event.target.files[0]);
  reader.onload = _event => {
    this.url = reader.result;
    console.log('-Evnet', _event);
    const storageRef = firebase.storage().ref();
    const fileName = '기관_'+ moment().format('x');
    const upRef = 'institutionImgs/' + fileName + '.png';
    const uploadTask = storageRef.child(upRef).putString(this.url, 'data_url');
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        console.log('snapshot', snapshot);
      },
      error => {
        console.log('error', error);
      },
      () => {
        firebase
          .storage()
          .ref()
          .child(upRef)
          .getDownloadURL()
          .then(         
            downloadURL => {
              this.img=downloadURL;
            },
            e => {
              console.log('error', e);
            }
          )
          .then(
            () => { },
            e => {
              console.log('error', e);
            }
          );
      }
    );
  };

}

//상담사사진 업로드
selectCslrFile(event) {
  var reader = new FileReader();
  reader.readAsDataURL(event.target.files[0]);
  reader.onload = _event => {
    this.url = reader.result;
    console.log('-Evnet', _event);
    const storageRef = firebase.storage().ref();
    const fileName = '상담사_'+ moment().format('x');
    const upRef = 'institutionImgs/' + fileName + '.png';
    const uploadTask = storageRef.child(upRef).putString(this.url, 'data_url');
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        console.log('snapshot', snapshot);
      },
      error => {
        console.log('error', error);
      },
      () => {
        firebase
          .storage()
          .ref()
          .child(upRef)
          .getDownloadURL()
          .then(         
            downloadURL => {
              this.counselor=downloadURL;
            },
            e => {
              console.log('error', e);
            }
          )
          .then(
            () => { },
            e => {
              console.log('error', e);
            }
          );
      }
    );
  };

}
}
