import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'app/services/common.service';
import { DbService } from 'app/services/db.service';
import { InstitutionService } from 'app/services/institution.service';
import * as firebase from 'firebase';
import * as moment from 'moment';

@Component({
  selector: 'app-institution-insert',
  templateUrl: './institution-insert.component.html',
  styleUrls: ['./institution-insert.component.scss']
})

//메뉴명 : 방문 기관 관리 > 기관 등록
export class InstitutionInsertComponent implements OnInit {
  @ViewChild("imageInput2", {static:true}) imageInput2:ElementRef;

  url;
  testKind;
  homepage;
  email;
  number;
  address;
  institution;
  name;
  img;
  counselor:any='https://firebasestorage.googleapis.com/v0/b/psychological-4c86e.appspot.com/o/institutionImgs%2Fcounselor.png?alt=media&token=81050efd-570c-44f7-83f4-b6a528c1760e';

  constructor(public router:Router,
              public db:DbService,
              private institutionService:InstitutionService,
              private cmmn : CommonService,) { 
      this.institution = this.institutionService.getInstitution();
    }
    
    ngOnInit() {
    }
  
    //목록으로 이동
    goInstitutionList(){
      alert('기관 등록을 취소합니다.');
      this.router.navigate(['/institution/institution-list']);
    }
  
    //저장
    fnSave(){
      if(!this.img){
        alert('기관사진을 등록하세요.');
        document.getElementById('img').focus();
        return;
      }else if(!this.name){
        alert('기관명을 입력하세요.');
        document.getElementById('name').focus();
        return;
      }else if(!this.testKind){
        alert('대표검사 종류를 입력하세요.');
        document.getElementById('testKind').focus();
        return;
      }else if(!this.number){
        alert('연락처를 입력하세요.');
        document.getElementById('number').focus();
        return;
      }else if(!this.address){
        alert('주소를 입력하세요.');
        document.getElementById('address').focus();
        return;
      }

      this.institution.docId=this.cmmn.generateFilename();
      this.institution.name = this.name;
      this.institution.kind = this.testKind;
      this.institution.homepage = this.homepage;
      this.institution.email = this.email;
      this.institution.pNumber = this.number;
      this.institution.address = this.address;
      this.institution.img=this.img
      this.institution.counselor=this.counselor
      this.institution.created=new Date().toISOString();
      this.db.updateAt(`institution/${this.institution.docId}`,this.institution).then(()=>{
        alert('저장 되었습니다. 목록으로 이동합니다.');
        this.router.navigate(['/institution/institution-list']);
        return;
      }).catch((error:any)=>{
        alert('새로고침 후 다시시도하세요.\n에러 발생 : ' + error);
        return;
      });
    }
    
    
//기관사진 선택
selectImg(){
  document.getElementById('visitUploader').click();
}

//상담사 사진 선택
selectCslrImg(){
  document.getElementById('cslrUploader').click();
}

//기관사진 업로드
selectFile(event) {
  var reader = new FileReader();
  reader.readAsDataURL(event.target.files[0]);
  reader.onload = _event => {
    this.url = reader.result;
    console.log('-Evnet', _event);
    const storageRef = firebase.storage().ref();
    const fileName = '기관_'+ moment().format('x');
    const upRef = 'institutionImgs/' + fileName + '.png';
    const uploadTask = storageRef.child(upRef).putString(this.url, 'data_url');
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        console.log('snapshot', snapshot);
      },
      error => {
        console.log('error', error);
      },
      () => {
        firebase
          .storage()
          .ref()
          .child(upRef)
          .getDownloadURL()
          .then(         
            downloadURL => {
              this.img=downloadURL;
            },
            e => {
              console.log('error', e);
            }
          )
          .then(
            () => { },
            e => {
              console.log('error', e);
            }
          );
      }
    );
  };

}

//상담사사진 업로드
selectCslrFile(event) {
  var reader = new FileReader();
  reader.readAsDataURL(event.target.files[0]);
  reader.onload = _event => {
    this.url = reader.result;
    console.log('-Evnet', _event);
    const storageRef = firebase.storage().ref();
    const fileName = '상담사_'+ moment().format('x');
    const upRef = 'institutionImgs/' + fileName + '.png';
    const uploadTask = storageRef.child(upRef).putString(this.url, 'data_url');
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        console.log('snapshot', snapshot);
      },
      error => {
        console.log('error', error);
      },
      () => {
        firebase
          .storage()
          .ref()
          .child(upRef)
          .getDownloadURL()
          .then(         
            downloadURL => {
              this.counselor=downloadURL;
            },
            e => {
              console.log('error', e);
            }
          )
          .then(
            () => { },
            e => {
              console.log('error', e);
            }
          );
      }
    );
  };

}
}
