import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionInsertComponent } from './institution-insert.component';

describe('InstitutionInsertComponent', () => {
  let component: InstitutionInsertComponent;
  let fixture: ComponentFixture<InstitutionInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
