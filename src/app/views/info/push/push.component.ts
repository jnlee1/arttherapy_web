import { DbService } from 'app/services/db.service';
import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';

@Component({
  selector: 'app-push',
  templateUrl: './push.component.html',
  styleUrls: ['./push.component.scss'],
  animations: egretAnimations,
})

//메뉴명 : 서비스/정보 관리 > 푸시관리
export class PushComponent implements OnInit {
  push$;
  public finish: any;
  public cant: any;

  constructor(
    private db: DbService,
  ) { }

  //푸시 멘트 가져오는 함수
  ngOnInit() {
    this.push$ = this.db.doc$(`master/push`).take(1).subscribe((terms: any) => {
      this.finish = terms.finish;
      this.cant = terms.cant;
    });
  }

  //결과서 작성완료시 뜨는 문구 수정하는 함수
  updateFinish(pri, ser) {
    this.db.updateAt(`master/push`, { finish: this.finish }).then((privacy) => {
      alert("업데이트 하였습니다.");
    });
  }

  //분석 실패시 뜨는 문구 수정하는 함수
  updateCant(pri, ser) {
    this.db.updateAt(`master/push`, { cant: this.cant }).then((privacy) => {
      alert("업데이트 하였습니다.");
    });
  }

}
