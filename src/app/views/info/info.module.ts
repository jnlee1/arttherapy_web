import { PushComponent } from './push/push.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedMaterialModule } from 'app/shared/shared-material.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { InfoRoutes } from './info.routing';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { DbService } from '../../services/db.service';
import { ColorSketchModule } from 'ngx-color/sketch';


import {
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatChipsModule,
  MatListModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSlideToggleModule
} from '@angular/material';
import { TermComponent } from './term/term.component';
import { PersonalComponent } from './personal/personal.component';
import { RefundComponent } from './refund/refund.component';
import { NgImageSliderModule } from 'ng-image-slider';

@NgModule({
  declarations: [
    TermComponent,
    PersonalComponent,
    RefundComponent,
    PushComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    Ng2ImgMaxModule,
    RouterModule.forChild(InfoRoutes),
    TranslateModule,
    NgbModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    ColorSketchModule,
    NgImageSliderModule
  ],
  providers: [DbService],
})
export class InfoModule { }
