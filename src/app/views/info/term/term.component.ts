import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, } from '@angular/material';
import { Subscription } from 'rxjs';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { DbService } from 'app/services/db.service';

export interface DialogData {
  type: string;
  item: string;

}

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  animations: egretAnimations
})

//메뉴명 : 서비스/정보 관리 > 이용약관
export class TermComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public items$: any;
  public serviceTerm: any;

  constructor(
    private db: DbService
  ) { }

  //DB에 저장되있는 약관을 불러오는 함수
  ngOnInit() {
    this.items$ = this.db.doc$(`master/term`).take(1).subscribe((terms: any) => {
      this.serviceTerm = terms.serviceTerm;
    });
  }

  //이용약관 수정시 저장되는 함수
  updateServiceTerm(pri, ser) {
    this.db.updateAt(`master/term`, { serviceTerm: this.serviceTerm }).then((privacy) => {
      alert("업데이트 하였습니다.");
    });
  }


}