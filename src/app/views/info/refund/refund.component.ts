import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, } from '@angular/material';
import { Subscription } from 'rxjs';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { DbService } from 'app/services/db.service';

export interface DialogData {
  type: string;
  item: string;

}

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 서비스/정보 관리 > 환불규정
export class RefundComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public items$: any;
  public refund :any;

  constructor(
    private db: DbService
  ) { }

  //DB에 저장되있는 약관을 불러오는 함수
  ngOnInit() {
    this.items$ = this.db.doc$(`master/term`).take(1).subscribe((terms: any) => {
      this.refund = terms.refund;
    });
  }

  //환불규정
  updateRefund(pri, ser) {
    this.db.updateAt(`master/term`, { refund: this.refund }).then((privacy) => {
      alert("업데이트 하였습니다.");
    });
  }

}