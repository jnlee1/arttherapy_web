import { PushComponent } from './push/push.component';
import { Routes } from '@angular/router';
import { TermComponent } from './term/term.component';
import { PersonalComponent } from './personal/personal.component';
import { RefundComponent } from './refund/refund.component';


export const InfoRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'term',
        component: TermComponent,
        data: { title: '이용약관', breadcrumb: '이용약관' }
      },
      {
        path: 'personal',
        component: PersonalComponent,
        data: { title: '개인정보', breadcrumb: '개인정보' }
      },
      {
        path: 'refund',
        component: RefundComponent,
        data: { title: '환불규정', breadcrumb: '환불규정' }
      },
      {
        path: 'push',
        component: PushComponent,
        data: { title: '푸쉬 관리', breadcrumb: '푸쉬 관리' }
      }
    ],
  },


];