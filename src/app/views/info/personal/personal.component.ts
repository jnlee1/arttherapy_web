import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, } from '@angular/material';
import { Subscription } from 'rxjs';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { DbService } from 'app/services/db.service';

export interface DialogData {
  type: string;
  item: string;

}

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 서비스/정보 관리 > 개인정보
export class PersonalComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public items$: any;
  public privacy: any;

  constructor(
    private db: DbService
  ) { }

  //DB에 저장되있는 약관을 불러오는 함수
  ngOnInit() {
    this.items$ = this.db.doc$(`master/term`).take(1).subscribe((terms: any) => {
      this.privacy = terms.privacy;
    });
  }

  //개인정보이용약관 수정시 저장되는 함수
  updatePrivacy(pri, ser) {
    this.db.updateAt(`master/term`, { privacy: this.privacy }).then((privacy) => {
      alert("업데이트 하였습니다.");
    });
  }

 
}