import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DbService } from 'app/services/db.service';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { AngularFirestore } from 'angularfire2/firestore';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-questionadmin',
  templateUrl: './questionadmin.component.html',
  styleUrls: ['./questionadmin.component.scss'],
  animations: egretAnimations
})

//메뉴명 : 검사 질문 관리 > 검사 질문 목록
export class QuestionadminComponent implements OnInit {
  questionList:[];
  items$;
  public items: any[];
  filterKind;
  filterSubKind;
  filterUse;
  filterTitle;
  docId;
  kind;
  subKind;
  use;
  kindList$;
  subKindlist$;
  

  useList=[
    {code:true, value : '사용'},
    {code:false, value : '미사용'},
  ]

  constructor(private router:Router,
             private db: DbService,
             public afs: AngularFirestore) {
             this.getQuestion();
             this.getKind();
              }

  ngOnInit() {
  }

  //대분류조회
  getKind(){
    this.db.collection$(`code`).pipe(take(1)).subscribe((data:any)=>{
        if(data){
          this.kindList$=data;
        }
    });
  }
  //HTP 소분류 조회
 getSubKind(){
   if(this.filterKind == 'HTP'){
      this.db.doc$(`code/HTP`).pipe(take(1)).subscribe((data:any)=>{
         if(data){
           this.subKindlist$ = data.subKind;
         }
      });
   }else{
    this.subKindlist$ =null;
    this.filterSubKind=null;
   }
 }

  //상세보기
  goDetail(params){
    this.router.navigate(['/question/questiondetail'],{
      queryParams:{
        docId:params.docId
      }
    });
  }

  //등록화면으로
  goInsert(){
    this.router.navigate(['/question/questioninsert']);
  }

  //모든 검사 질문 검색
  getQuestion(){
    try{
      this.items$ = this.db.collection$(`question`, ref => ref.orderBy("created", "desc"))
    }catch(e){
      alert('에러발생 : '+e);
      return;
    }
    
  }

  //대분류에 따른 질문목록 조회
  changedKind(){
      try{
        this.items$ = this.db.collection$(`question`, ref => ref.where('kind','==',this.filterKind)
                                                                .orderBy('created','desc'))

       //소분류 코드 조회 넣을 예정
      }catch(e){
        alert('에러발생 : '+e);
         //에러 발생하면 전체조회로
         this.getQuestion();
      }
    
  }

  //소분류에 따른 질문목록 조회
  selectSub(){
    try{
      this.items$ = this.db.collection$(`question`, ref => ref.where('subKind','==',this.filterSubKind)
                                                              .orderBy('created','desc'))

      //소분류 코드 조회 넣을 예정
    }catch(e){
      alert('에러발생 : '+e);
        //에러 발생하면 전체조회로
        this.getQuestion();
    }
  
}


  //사용여부에 따른 질문목록 조회
  selectUse(){
    try{
      this.items$ = this.db.collection$(`question`, ref => ref.where('use','==',this.filterUse)
                                                              .orderBy('created','desc'))

      //소분류 코드 조회 넣을 예정
    }catch(e){
      alert('에러발생 : '+e);
        //에러 발생하면 전체조회로
        this.getQuestion();
    }
  
}


   //대,소분류에 따른 질문목록 조회
   changedSubKind(){
      try{
        this.items$ = this.db.collection$(`question`, ref => ref.where('kind','==',this.filterKind)
                                                                .where('subKind','==',this.filterSubKind)
                                                                .orderBy('created','desc'))
      }catch(e){
        alert('에러발생 : '+e);
        //에러 발생하면 전체조회로
        this.getQuestion();
      }
    
    }

      //대분류,사용여부에 따른 질문목록 조회
      selectKindUseFilter(){
        try{
          this.items$ = this.db.collection$(`question`, ref => ref.where('kind','==',this.filterKind)
                                                                  .where('use','==',this.filterUse)
                                                                  .orderBy('created','desc'))

        //소분류 코드 조회 넣을 예정
        }catch(e){
          alert('에러발생 : '+e);
          //에러 발생하면 전체조회로
          this.getQuestion();
        }
      
    }

     //대분류,사용여부 따른 질문목록 조회
     changedUse(){
      try{
          this.items$ = this.db.collection$(`question`, ref => ref.where('kind','==',this.filterKind)
                                                                  .where('subKind','==',this.filterSubKind)
                                                                  .where('use','==',this.filterUse)
                                                                  .orderBy('created','desc'))
          // this.items$ = this.db.collection$(`question`, ref => ref.where('use','==',this.filterUse)
          //                                                         .orderBy('created','desc'))
      }catch(e){
        alert('에러발생 : '+e);
        //에러 발생하면 전체조회로
        this.getQuestion();
      }
    
    }


    //대분류,사용여부에 따른 질문목록 조회
    selectSubUseFilter(){
      try{
        this.items$ = this.db.collection$(`question`, ref => ref.where('subKind','==',this.filterSubKind)
                                                                .where('use','==',this.filterUse)
                                                                .orderBy('created','desc'))

      }catch(e){
        alert('에러발생 : '+e);
        //에러 발생하면 전체조회로
        this.getQuestion();
      }
      
    }


  //조건에 따른 질문목록 조회
  selectQuestion(){

    if(this.filterKind != null){//대분류가 있을경우
          if(this.filterSubKind  != null){//소분류가 있을경우
              if(this.filterUse  != null){//사용여부가 있을경우
                this.changedUse();
              }else{
                this.changedSubKind();
              }
          }else if(this.filterUse  != null){
                this.selectKindUseFilter();
          }else{
                this.changedKind();
          }
    }else if(this.filterSubKind  != null){
              if(this.filterUse  != null){
                    this.selectSubUseFilter();
              }else{
                    this.selectSub();
              }
    }else if(this.filterUse  != null){
              this.selectUse();
    }else{
            this.getQuestion();
    }




  }

  // changeChk(id, chk, kind, subKind){
  //     this.docId = id;
  //     this.kind = kind;
  //     this.subKind = subKind;
  //     this.use = !chk;
  //     this.db.updateAt(`question/${this.docId}`,{use:this.use});

  //   this.checkUse();

  // }

  // checkUse(){
  //   try {
  //     if(this.kind == 'HTP'){
  //       this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
  //                                              .where('subKind','==',this.subKind)
  //                                              .where('use','==',true))
  //             .pipe(take(1))
  //             .subscribe(response =>{
  //               if(response[0]){
  //                 alert('선택한 설문지를 사용합니다.');
  //               }else{
  //                 alert('최소 한 개 설문지를 사용해야합니다.');
  //               }
  //               return;
  //             })
  //     }else{
  //       this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
  //                                              .where('use','==',true))
  //             .pipe(take(1))
  //             .subscribe(response =>{
  //               if(response[0]){
  //                 alert('선택한 설문지를 사용합니다.');
  //               }else{
  //                 alert('최소 한 개 설문지를 사용해야합니다.');
  //               }
  //               return;
  //             })
  //     }
  //   } catch (error) {
  //     alert(error);
  //     return;          
  //   }
  // }
  






}
