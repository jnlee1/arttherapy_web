import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'app/services/common.service';
import { DbService } from 'app/services/db.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-question-update',
  templateUrl: './question-update.component.html',
  styleUrls: ['./question-update.component.scss']
})

//메뉴명 : 검사 질문 관리 > 검사 질문 수정
export class QuestionUpdateComponent implements OnInit {
  docId;
  questionList$;
  kind;
  subKind;
  question;
  date: String = new Date().toISOString();
  no;
  title;

  constructor(public router : Router,
              public route:ActivatedRoute,
              public db:DbService,
              public cmmn : CommonService) {

      this.route.queryParams.subscribe(params => {
          this.docId = params["docId"];
          this.getQuestionList();
      });
  }

  ngOnInit() {
  }

  //질문 조회
  getQuestionList(){
    this.db.doc$(`question/${this.docId}`).pipe(take(1)).subscribe((question:any)=>{
                this.kind = question.kind;
                this.subKind = question.subKind;
                this.title = question.title;
                this.questionList$ = question.questionList;
            });
  }

  //질문추가
  addQuestion(param){
    if(param){
      this.questionList$.push(param);
      this.question='';
    }else{
      alert('질문 내용을 입력하세요.');
      return;
    }

  }

  //질문삭제
  deleteQuestion(param){
    for (var i = this.questionList$.length; i--;) {
      if (i === param) {
        this.questionList$.splice(i, 1);
      }
    }
  }

  //목록으로 이동
  goQuestionList(){
    this.router.navigate(['/question/questionadmin']);
  }

  //질문저장
  saveQuestion(){
    if(this.questionList$.length > 0){
        this.db.updateAt(`question/${this.docId}`,{
                                    updated:this.date,
                                    title:this.title,
                                    questionList:this.questionList$})
               .then((privacy)=>{
                    alert('저장되었습니다.');
                    //저장후 목록으로 이동
                    this.router.navigate(['/question/questionadmin']);
        });
    }else{
      alert('질문이 없습니다. 질문추가 후 저장하세요.');
      return;
    }
  }

}
