import { Routes } from '@angular/router';
import { QuestionadminComponent } from './questionadmin/questionadmin.component';
import { QuestionInsertComponent } from './question-insert/question-insert.component';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionUpdateComponent } from './question-update/question-update.component';


export const QuestionRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'questionadmin',
        component: QuestionadminComponent,
        data: { title: '검사 질문 관리', breadcrumb: '검사 질문 관리' }
      },
    ],
  },
  {
    path: '',
    children: [
      {
        path: 'questioninsert',
        component: QuestionInsertComponent,
        data: { title: '검사 질문 등록', breadcrumb: '검사 질문 등록' }
      },
    ],
  },
  {
    path: '',
    children: [
      {
        path: 'questiondetail',
        component: QuestionDetailComponent,
        data: { title: '검사 질문 상세 보기', breadcrumb: '검사 질문 상세 보기' }
      },
    ],
  },
  {
    path: '',
    children: [
      {
        path: 'questionupdate',
        component: QuestionUpdateComponent,
        data: { title: '검사 질문 수정', breadcrumb: '검사 질문 수정' }
      },
    ],
  },


];