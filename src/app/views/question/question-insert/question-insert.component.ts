import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'app/services/common.service';
import { DbService } from 'app/services/db.service';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-question-insert',
  templateUrl: './question-insert.component.html',
  styleUrls: ['./question-insert.component.scss']
})

//메뉴명 : 검사 질문 관리 > 질문 등록
export class QuestionInsertComponent implements OnInit {

  questionList=[];
  question;
  kind;
  subKind;
  isDisabled=false;
  date: String = new Date().toISOString();
  docId;
  title;
  id;
  kindList$;
  subKindlist$

  constructor(
    private router:Router,
    private db: DbService,
    private cmmn : CommonService
  ) {
    this.getKind();
   }

  ngOnInit() {
  }

  //대분류조회
  getKind(){
    this.db.collection$(`code`).pipe(take(1)).subscribe((data:any)=>{
        if(data){
          this.kindList$=data;
        }
    });
  }
  //HTP 소분류 조회
  getSubKind(){
    if(this.kind == 'HTP'){
       this.db.doc$(`code/HTP`).pipe(take(1)).subscribe((data:any)=>{
          if(data){
            this.subKindlist$ = data.subKind;
          }
       });
    }else{
     this.subKindlist$ =null;
     this.subKind=null;
    }
  }

  //질문추가
  addQuestion(param){
    if(this.kind == 'HTP'){
      if(!this.kind || !this.subKind){
        alert('대/소분류를 선택하고 내용을 추가하세요.');
        return;
      }
    }else{
      if(!this.kind){
        alert('대분류를 선택하고 내용을 추가하세요.');
        return;
      }
    }

    if(param){
      this.questionList.push(param);
      this.question='';
    }else{
      alert('질문 내용을 입력하세요.');
      return;
    }

  }

  //목록으로 이동
  goQuestionList(){
    alert('질문 등록이 취소 되었습니다.\n목록으로 이동합니다.');
    this.router.navigate(['/question/questionadmin']);
  }

  //질문삭제
  deleteQuestion(param){
    for (var i = this.questionList.length; i--;) {
      if (i === param) {
        this.questionList.splice(i, 1);
      }
    }

    //질문내용이 없으면 분류 활성화
    if(this.questionList.length == 0){
      this.isDisabled=false;
    }
  }

  //질문저장
  saveQuestion(){
    if(this.title == null){
      alert('질문 제목을 입력하세요.');
      document.getElementById('titleQue').focus();
      return;
    }

    if(this.questionList.length > 0){
        this.getQuestionCount();
    }else{
      alert('질문이 없습니다. 질문추가 후 저장하세요.');
      return;
    }
  }

  //대소분류 따른 개수 조회 (예)1차,2차
  getQuestionCount(){
    this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
                                           .where('subKind','==',this.subKind)
                                           .where('use','==',true))
            .pipe(take(1)).subscribe((items:any)=>{
                if(items.length > 0){
                  this.id = items[0].id;
                  this.db.updateAt(`question/${this.id}`,{use:false})
                }
                this.insertQuestion();
            });
  }

  //질문추가
  insertQuestion(){
    this.docId=this.cmmn.generateFilename();
    this.db.updateAt(`question/${this.docId}`,{
                                docId:this.docId,
                                kind:this.kind,
                                subKind:this.subKind,
                                created:this.date,
                                questionList:this.questionList,
                                title:this.title,
                                use:true})
            .then((privacy)=>{
      alert('저장되었습니다.');
      //저장후 목록으로 이동
      this.router.navigate(['/question/questionadmin']);
    })
  }

}
