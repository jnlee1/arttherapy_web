import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DbService } from 'app/services/db.service';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})

//메뉴명 : 검사질문관리 > 질문 상세 보기
export class QuestionDetailComponent implements OnInit {

  items$;
  docId;
  questionList$;
  kind;
  subKind;
  title;
  use;

  constructor(public router:Router,
              public route:ActivatedRoute,
              public db:DbService) { 


    this.route.queryParams.subscribe(params => {
      this.docId = params["docId"];
      this.getQuestionList();
    });
    
  }
  
  ngOnInit() {
  }


  getQuestionList(){
    this.db.doc$(`question/${this.docId}`).pipe(take(1)).subscribe((question:any)=>{
                this.kind = question.kind;
                this.subKind = question.subKind;
                this.title = question.title;
                this.use = question.use;
                this.questionList$ = question.questionList;
            });
  
  }

  //목록으로 이동
  goQuestionList(){
    this.router.navigate(['/question/questionadmin']);
  }

  //수정화면으로 이동
  goQuestionUpdate(){
    this.router.navigate(['/question/questionupdate'],{
      queryParams:{
        docId:this.docId,
        title:this.title
      }
    });
  }

  //수정체크
  changeChk(){
    if(!this.use){
      debugger;

      if(this.kind == 'HTP'){
        this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
                                              .where('subKind','==',this.subKind)
                                              .where('use','==',true))
              .pipe(take(1)).subscribe((items:any)=>{
              if(items.length > 0){
                var result = confirm("다른 질문지를 사용중입니다.\n방금 선택한 질문지를 사용하겠습니까?");
                if(result){
                    alert("해당 질문을 사용합니다.");
                    this.db.updateAt(`question/${items[0].docId}`,{use:false});
                    this.db.updateAt(`question/${this.docId}`,{use:true});
                }else{
                    alert("취소되었습니다.");
                    this.use=false;
                    return;
                }
              }else{
                alert('해당 질문을 사용합니다.');
                this.db.updateAt(`question/${this.docId}`,{use:true});
              }
              });
      }else{
        this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
                                              .where('use','==',true))
              .pipe(take(1)).subscribe((items:any)=>{
              if(items.length > 0){
                var result = confirm("다른 질문지를 사용중입니다.\n방금 선택한 질문지를 사용하겠습니까?");
                if(result){
                    alert("해당 질문을 사용합니다.");
                    this.db.updateAt(`question/${items[0].docId}`,{use:false});
                    this.db.updateAt(`question/${this.docId}`,{use:true});
                }else{
                    alert("취소되었습니다.");
                    this.use=false;
                    return;
                }
              }else{
                alert('해당 질문을 사용합니다.');
                this.db.updateAt(`question/${this.docId}`,{use:true});
              }
              });
      }

    }else{
      if(this.kind == 'HTP'){
        this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
                                              .where('subKind','==',this.subKind)
                                              .where('use','==',true))
              .pipe(take(1)).subscribe((items:any)=>{
                  if(items[0].docId != this.docId){
                    this.db.updateAt(`question/${items[0].docId}`,{use:false});
                  }else{
                    alert('사용할 질문지가 없으므로 해당 질문지를 계속 사용합니다.');
                    this.use = true;
                    return;
                  }
              });
      }else{
        this.db.collection$(`question`,ref=>ref.where('kind','==',this.kind)
                                              .where('use','==',true))
              .pipe(take(1)).subscribe((items:any)=>{
                if(items[0].docId != this.docId){
                    this.db.updateAt(`question/${items[0].docId}`,{use:false});
                  }else{
                    alert('사용할 질문지가 없으므로 해당 질문지를 계속 사용합니다.');
                    this.use = true;
                    return;
                  }
              });
      }

      //this.db.updateAt(`question/${this.docId}`,{use:false});
    }
  }

  
}
