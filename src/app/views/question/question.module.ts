import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedMaterialModule } from 'app/shared/shared-material.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { 
  MatInputModule,
  MatIconModule, 
  MatCardModule, 
  MatMenuModule, 
  MatButtonModule, 
  MatChipsModule, 
  MatListModule, 
  MatTooltipModule, 
  MatDialogModule,
  MatSnackBarModule, 
  MatSlideToggleModule } from '@angular/material';
import { QuestionRoutes } from './question.routing';
import { QuestionadminComponent } from './questionadmin/questionadmin.component';
import { QuestionInsertComponent } from './question-insert/question-insert.component';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionUpdateComponent } from './question-update/question-update.component';



@NgModule({
  declarations: [
    QuestionadminComponent,
    QuestionInsertComponent,
    QuestionDetailComponent,
    QuestionUpdateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(QuestionRoutes),
    NgxDatatableModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    TranslateModule,
    NgbModule,
  ]
})
export class QuestionModule { }
