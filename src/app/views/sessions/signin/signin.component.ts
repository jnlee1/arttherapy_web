import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { AuthService } from 'app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  animations: egretAnimations
})
export class SigninComponent implements OnInit {

  signupForm: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {
    this.checkMaster();
  }

  ngOnInit() {

    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signupForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: password,
        agreed: [false, Validators.required]
      }
    );

  }

  onSubmit() {
    if (!this.signupForm.invalid) {
      this.auth.signIn(this.signupForm.value);
      // do what you wnat with your data
      console.log(this.signupForm.value);
    }
  }

  async checkMaster() {
    const master = await this.auth.masters();

    if (!master) {
      this.router.navigate(['sessions/signup']);
    }

  }

}
