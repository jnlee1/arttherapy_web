import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { map, delay, switchMap, tap } from 'rxjs/operators';
import { Observable, of, defer, combineLatest } from 'rxjs';
import 'rxjs/add/operator/take';

export const leftJoinDocument = (afs: AngularFirestore, field, collection) => {
  return (source) =>
    defer(() => {
      // Operator state
      let collectionData;
      const cache = new Map();

      return source.pipe(
        switchMap((data) => {
          // Clear mapping on each emitted val ;
          cache.clear();

          // Save the parent data state
          collectionData = data as any[];

          const reads$ = [];
          let i = 0;
          for (const doc of collectionData) {
            // Skip if doc field does not exist or is already in cache
            if (!doc[field] || cache.get(doc[field])) {
              continue;
            }

            // Push doc read to Array
            reads$.push(afs.collection(collection).doc(doc[field]).valueChanges());
            cache.set(doc[field], i);
            i++;
          }

          return reads$.length ? combineLatest(reads$) : of([]);
        }),
        map((joins) => {
          return collectionData.map((v, i) => {
            const joinIdx = cache.get(v[field]);
            return {
              ...v,
              [field]: { ...joins[joinIdx], id: v[field] } || null,
            };
          });
        }),
        tap((final) => console.log(`Queried ${(final as any).length}, Joined ${cache.size} docs`))
      );
    });
};
@Injectable({
  providedIn: 'root'
})
export class DbService {

  items: any[];
  constructor(public afs: AngularFirestore, public angularDb: AngularFireDatabase) {
  }

  valueChanges(path) {
    return this.afs.collection(path).valueChanges();
  }

  collection$(path, query?) {
    return this.afs
      .collection(path, query)
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data: Object = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  doc$(path): Observable<any> {
    return this.afs
      .doc(path)
      .snapshotChanges()
      .pipe(
        map((doc: any) => {
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );
  }

  /**
   * @param  {string} path 'collection' or 'collection/docID'
   * @param  {object} data new data
   *
   * Creates or updates data on a collection or document.
   **/
  updateAt(path: string, data: Object): Promise<any> {
    const segments = path.split('/').filter(v => v);
    if (segments.length % 2) {
      // Odd is always a collection
      return this.afs.collection(path).add(data);
    } else {
      // Even is always document
      return this.afs.doc(path).set(data, { merge: true }).then(() => {
        console.log('updateSucs');
      }).catch((error) => {
        console.log('error', error);
      });
    }
  }

  /**
   * @param  {string} path path to document
   *
   * Deletes document from Firestore
   **/
  delete(path) {
    return this.afs.doc(path).delete();
  }


  // fireDatabase
  getNotices() {
    return this.angularDb.list('/notices');
  }

  getFaq() {
    return this.angularDb.list('/faqs');
  }

  getInfo() {
    return this.angularDb.list('/neighborInfo');
  }

  getFamily() {
    return this.angularDb.list('/accounts', query => {
      return query.orderByChild('type').equalTo('family');
    });
  }

  getCareWorker() {
    return this.angularDb.list('/accounts', query => {
      return query.orderByChild('type').equalTo('careWorker');
    });
  }

  getFacility() {
    return this.angularDb.list('/accounts', query => {
      return query.orderByChild('type').equalTo('facility');
    });
  }

  getMaster() {
    return this.angularDb.list('/master');
  }

  getMasterUser() {
    return this.angularDb.object('/master/master001/term');
  }

  addMaster(item): Observable<any> {
    item.id = Math.round(Math.random() * 10000000000).toString();
    this.getNeighborInfoById(item.id).update(item).then(test => {
      console.log('db.service addItem TEST :', test);
    });
    return this.getInfo().valueChanges().take(1);
  }

  updateMasterPri(item): Observable<any> {
    this.getMasterUser().update({ privacy: item }).then(data => {
      console.log('update DB', data);
    });
    return this.getMaster().valueChanges().take(1);
  }
  updateMasterSer(item): Observable<any> {
    this.getMasterUser().update({ serviceTerm: item }).then(data => {
      console.log('update DB', data);
    });
    return this.getMaster().valueChanges().take(1);
  }

  getRequestFacility() {
    return this.angularDb.list('/requestFacility');
  }

  getInquiry() {
    return this.angularDb.list('/inquirys');
  }

  getUser(userId) {
    return this.angularDb.object('/accounts/' + userId);
  }

  getUsers() {
    return this.angularDb.list('/accounts/');
  }

  getUserDetail(userId) {
    return this.angularDb.object('/accountsDetail/' + userId);
  }

  getInquiryByid(inquiryId) {
    return this.angularDb.object('/inquirys/' + inquiryId);
  }

  // coverU 정보저장
  getNeighborInfoById(neighborId: string) {
    return this.angularDb.object('neighborInfo/' + neighborId);
  }

  getFaqInfoById(FaqId: string) {
    return this.angularDb.object('faqs/' + FaqId);
  }

  getNoticeInfoById(NoticeId: string) {
    return this.angularDb.object('notices/' + NoticeId);
  }


  // firebase 데이터 추가 함수

  addInfoItem(item): Observable<any> {
    item.id = Math.round(Math.random() * 10000000000).toString();
    this.getNeighborInfoById(item.id).update(item).then(test => {
      console.log('db.service addItem TEST :', test);
    });
    return this.getInfo().valueChanges().take(1);
  }

  addFaQItem(item): Observable<any> {
    item.id = Math.round(Math.random() * 10000000000).toString();
    this.getFaqInfoById(item.id).update(item).then(test => {
      console.log('db.service addItem TEST :', test);
    });
    return this.getFaq().valueChanges().take(1);
  }

  addNoticeItem(item): Observable<any> {
    item.id = Math.round(Math.random() * 10000000000).toString();
    this.getNoticeInfoById(item.id).update(item).then(test => {
      console.log('db.service addItem TEST :', test);
    });
    return this.getNotices().valueChanges().take(1);
  }


  // firebase 데이터 수정 함수

  updateInfoItem(id, item): Observable<any> {
    this.getNeighborInfoById(id).update(item).then(data => {
      console.log('update DB', data);
      if (data === id) {
        return Object.assign({}, data, item);
      }
      return data;
    });
    return this.getInfo().valueChanges().take(1);
  }

  updateFaQItem(id, item): Observable<any> {
    this.getFaqInfoById(id).update(item).then(data => {
      console.log('update DB', data);
      if (data === id) {
        return Object.assign({}, data, item);
      }
      return data;
    });
    return this.getFaq().valueChanges().take(1);
  }

  updateNoticeItem(id, item): Observable<any> {
    this.getNoticeInfoById(id).update(item).then(data => {
      console.log('update DB', data);
      if (data === id) {
        return Object.assign({}, data, item);
      }
      return data;
    });
    return this.getNotices().valueChanges().take(1);
  }


  // firebase 데이터 삭제 함수

  removeInfoItem(row): Observable<any> {
    this.getNeighborInfoById(row.id).remove().then(deletedb => {
      console.log('delete DB', row);
    });
    return this.getInfo().valueChanges().take(1);
  }

  removeFaQItem(row): Observable<any> {
    this.getFaqInfoById(row.id).remove().then(deletedb => {
      console.log('delete DB', row);
    });
    return this.getFaq().valueChanges().take(1);
  }

  removeNoticeItem(row): Observable<any> {
    this.getNoticeInfoById(row.id).remove().then(deletedb => {
      console.log('delete DB', row);
    });
    return this.getNotices().valueChanges().take(1);
  }
}
