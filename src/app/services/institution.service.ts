import { Injectable } from "@angular/core";

@Injectable({
    providedIn:'root'
})
export class InstitutionService{
    institution:any={
        address:'',
        email:'',
        homepage:'',
        kind:'',
        pNumber:'',
    }
    constructor(){}

    setInstitution(institution){
        this.institution = institution;
    }

    getInstitution(){
        return this.institution;
    }

}