import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

import { Observable, of } from 'rxjs';
import { switchMap, take, map } from 'rxjs/operators';
import { DbService } from './db.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<any>;
  master$: Observable<any>;

  counselorUser$: Observable<any>;
  counselorMaster$: Observable<any>;

  constructor(
    private afAuth: AngularFireAuth,
    private db: DbService,
    private router: Router,
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => (user ? db.doc$(`master/${user.uid}`) : of(null)))
    );

    this.master$ = this.db.collection$('master').pipe(
      switchMap(m => (m.length > 0 ? m : of(null)))
    );

    this.counselorUser$ = this.afAuth.authState.pipe(
      switchMap(user => (user ? db.doc$(`master-counselor/${user.uid}`) : of(null)))
    );

    this.counselorMaster$ = this.db.collection$('master-counselor').pipe(
      switchMap(m => (m.length > 0 ? m : of(null)))
    );

  }

  uid() {
    return this.user$
      .pipe(
        take(1),
        map(u => u && u.uid)
      )
      .toPromise();
  }

  masters() {
    return this.master$.pipe(
      take(1),
      map(m => m)
    )
      .toPromise();

  }

  counselorUid() {
    return this.counselorUser$
      .pipe(
        take(1),
        map(u => u && u.uid)
      )
      .toPromise();
  }

  counselorMasters() {
    return this.counselorMaster$.pipe(
      take(1),
      map(m => m)
    )
      .toPromise();

  }


  private createMasterData({ uid, email, displayName, photoURL, isAnonymous }, userName) {
    // Sets user data to firestore on login

    const path = `master/${uid}`;

    const data = {
      uid,
      email,
      userName,
      photoURL,
    };

    return this.db.updateAt(path, data);
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    return this.router.navigate(['sessions/signin']);
  }


  async signIn(obj) {
    await this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password).then(() => console.log('success')
    ).catch((error) => console.log('error', error)
    );
    return this.router.navigate(['/']);
  }

  


  async signUp(obj) {
    const credential = await this.afAuth.auth.createUserWithEmailAndPassword(obj.email, obj.password);
    return await this.createMasterData(credential.user, obj.username);
  }


}
