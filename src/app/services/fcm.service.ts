/** @format */

import { Injectable } from '@angular/core';

import { DbService } from './db.service';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FcmService {
  constructor(
    public db: DbService,
    public auth: AuthService,
    public httpClient: HttpClient
  ) { }

  sendPage(pushId: string, title: string, message: string, counselorId: string) {
    return new Promise((resolve, reject) => {
      if (pushId) {
        let notification = {};
        notification = {
          notification: {
            title: title,
            body: message,
            click_action: 'FCM_PLUGIN_ACTIVITY',
            isScheduled: 'false',
            sound: 'default',
          },
          data: {
            counselorId: counselorId
          },
          to: pushId,
          priority: 'high',
        };

        const fbkey =
          'AAAAo2b-95I:APA91bHTc2JloOqcRfxgvlhrHSGwuPinn_ewZDNYoPi4oYUylt3GBvvBeqeo7LoCSTPQPDB725TXkuNEusP2QtEtfVcroYtdWw6m6OX2lpzFuwTC1FVeOiVhN266jdK_Xi53yg6H14NT';

        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: `key=${fbkey}`,
          }),
        };

        this.httpClient
          .post(
            'https://fcm.googleapis.com/fcm/send',
            notification,
            httpOptions
          )
          .subscribe(
            (new_data) => {
              console.log('result', new_data);
              resolve(new_data);
            },
            (error) => {
              reject(error);
            }
          );
      } else {
        resolve('pushId error');
      }
    });
  }

  sendFcm(pushId: string, title: string, message: string) {
    return new Promise((resolve, reject) => {
      let notification = {};
      notification = {
        notification: {
          title: title,
          body: message,
          sound: 'default',
        },
        android: {
          webpush: {
            click_action: 'FCM_PLUGIN_ACTIVITY',
          },
        },

        to: pushId,
        priority: 'high',
      };

      const fbkey =
        'AAAAo2b-95I:APA91bHTc2JloOqcRfxgvlhrHSGwuPinn_ewZDNYoPi4oYUylt3GBvvBeqeo7LoCSTPQPDB725TXkuNEusP2QtEtfVcroYtdWw6m6OX2lpzFuwTC1FVeOiVhN266jdK_Xi53yg6H14NT';

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: `key=${fbkey}`,
        }),
      };

      this.httpClient.post('https://fcm.googleapis.com/fcm/send', notification, httpOptions).subscribe(
        new_data => {
          console.log('result', new_data);
          resolve(new_data);
        },
        error => {
          reject(error);
        }
      );
    });
  }
  /**
   * pageFCM
   * @param pushId pushId
   * @param message 메세지
   * @param user 스위치떄문에
   * @param page 만약 이동한다면
   */
  pageFcm(pushId: string, title: string, message: string, page?) {
    return new Promise((resolve, reject) => {
      let notification = {};
      notification = {
        notification: {
          title: title,
          body: message,
          click_action: 'FCM_PLUGIN_ACTIVITY',
          sound: 'default',
        },
        data: {
          page: page,
          type: 'page',
        },
        to: pushId,
        priority: 'high',
      };

      const fbkey =
        'AAAAo2b-95I:APA91bHTc2JloOqcRfxgvlhrHSGwuPinn_ewZDNYoPi4oYUylt3GBvvBeqeo7LoCSTPQPDB725TXkuNEusP2QtEtfVcroYtdWw6m6OX2lpzFuwTC1FVeOiVhN266jdK_Xi53yg6H14NT';

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: `key=${fbkey}`,
        }),
      };

      this.httpClient
        .post('https://fcm.googleapis.com/fcm/send', notification, httpOptions)
        .subscribe(
          (new_data) => {
            console.log('result', new_data);
            resolve(new_data);
          },
          (error) => {
            reject(error);
          }
        );
    });
  }
}
