import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FacilityFirestoreService {

  constructor(public http: HttpClient,
    public firestore: AngularFirestore
  ) {
    console.log('Hello FacilityFirestoreProvider Provider');
  }

  /////////////////// 기본 시설 정보 ///////////////////////
  getFacilityDetail(facilityId: string) {
    return this.firestore.collection("generalStatusDetailInfoItem", ref => ref.where("longTermAdminSym", "==", facilityId));
  }

  getInsttStatusDetailInfoItem(facilityId: number, adminPttn: string) {
    return this.firestore.collection("insttStatusDetailInfoItem", ref => ref.where("longTermAdminSym", "==", facilityId).where("adminPttn", "==", adminPttn));
  }

  getStaffStatusDetailInfoItem(facilityId: number, adminPttn: string) {
    return this.firestore.collection("staffStatusDetailInfoItem", ref => ref.where("longTermAdminSym", "==", facilityId).where("adminPttn", "==", adminPttn));
  }

  getFacilityPrograms(facilityId: string) {
    return this.firestore.collection("generalStatusDetailInfoItem").doc(facilityId).collection("programs", ref => ref.where("id", ">" , "")); // id가 있는 리스트 전부
  }

  getFacilityNonBenefits(facilityId: string) {
    return this.firestore.collection("generalStatusDetailInfoItem").doc(facilityId).collection("nonBenefits", ref => ref.where("id", ">" , "")); // id가 있는 리스트 전부
  }

  getFacilitySimple(facilityId: string) {
    return this.firestore.collection("generalStatusInfoItem", ref => ref.where("longTermAdminSym", "==", facilityId));
  }

  ////////////// 검색을 위한 쿼리 이용 시설 정보 ////////////////////
  searchFacilityNameQuery(search: string) {
    return this.firestore.collection("generalStatusInfoItem", ref => ref.where("adminNm", "==", search));
  }

  searchFacilityAddressQuery(search: string) {
    return this.firestore.collection("generalStatusInfoItem", ref => ref.where("addr_op", "==", search));
  }
  
}
