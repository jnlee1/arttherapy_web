import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
declare const require: any;

@Injectable({
  providedIn: 'root'
})
export class PushService {

  constructor(public httpClient: HttpClient) { }

  sendPush2(pushMessage, playerIds) {

    var sendNotification = (data) => {

      var headers = {
        "Content-Type": "application/json; charset=utf-8"
      };
      var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
      };
      var https = require('https');
      var req = https.request(options, function (res) {
        res.on('data', function (data) {
          console.log("Response:");
          console.log(JSON.parse(data));
        });
      });
      req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
      });
      req.write(JSON.stringify(data));
      req.end();
    };
    var message = {
      app_id: "09895cbd-b26f-43c9-b77d-93cda50fc438",
      contents: { "en": pushMessage },
      include_player_ids: playerIds
    };
    sendNotification(message);
  }

  sendPush(pushMessage, playerIds) {
    return new Promise((resolve, reject) => {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json; charset=utf-8"
        })
      };
      var message = {
        app_id: "09895cbd-b26f-43c9-b77d-93cda50fc438",
        contents: { "en": pushMessage },
        include_player_ids: playerIds
      };
      this.httpClient.post('https://onesignal.com/api/v1/notifications', message, httpOptions).subscribe(new_data => {
        console.log('result', new_data);
        resolve(new_data);
      }, error => {
        reject(error)
      });
    })

  }


}
