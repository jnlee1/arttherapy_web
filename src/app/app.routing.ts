import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './shared/components/layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './shared/components/layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './shared/services/auth/auth.guard';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'dashboard/default',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'sessions',
        loadChildren: () => import('./views/sessions/sessions.module').then(m => m.SessionsModule),
        data: { title: 'Session' }
      }
    ]
  },

  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'users',
        loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule),
        data: { title: '고객 서비스', breadcrumb: '고객 서비스' }
      },
      {
        path: 'consult',
        loadChildren: () => import('./views/consult/consult.module').then(m => m.ConsultModule),
        data: { title: '분석 관리', breadcrumb: '분석 관리' }
      },
      {
        path: 'info',
        loadChildren: () => import('./views/info/info.module').then(m => m.InfoModule),
        data: { title: '정보 서비스', breadcrumb: '정보 서비스' }
      },
      {
        path: 'question',
        loadChildren: () => import('./views/question/question.module').then(m => m.QuestionModule),
        data: { title: '검사 질문 관리', breadcrumb: '검사 질문 관리' },
        
      },
      {
        path: 'institution',
        loadChildren: () => import('./views/institution/institution.module').then(m => m.InstitutionModule),
        data: { title: '방문 기관 관리', breadcrumb: '방문 기관 관리' },
        
      },
    ]
  },


  {
    path: '**',
    redirectTo: 'sessions/404'
  }
];

